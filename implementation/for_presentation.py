import merkle_tree_2 as mt
import os


def merkle_tree_from_dir(dirname):
    mt1 = mt.MerkleTree()
    for root, dirs, files in os.walk(dirname):
        for file in files:
            mt1.add_from_file(file, os.path.join(root, file))
    mt1.build_tree()
    return mt1

print(20*'-')
print("COMPUTER B")
first = merkle_tree_from_dir("./testfiles")
print("Total no. of hashes:",len(first.hashes))
print("Total no. of files:",len(first.name_to_hash))
print("Root hash:", first.root_hash)

print(20*'-')
print("COMPUTER A")
second = merkle_tree_from_dir("./testfiles2")
print("Total no. of hashes:",len(second.hashes))
print("Total no. of files:",len(second.name_to_hash))
print("Root hash:", second.root_hash)
print(20*'-')
print("PROTOCOL SIMULATION")
invalid_leaf = mt.protocol_simulation(second, first)
print(2*"\n")

if invalid_leaf:
    invalid_file = ""
    for leaf in invalid_leaf:
        for key, value in second.name_to_hash.items():
            if value == leaf:
                print("Invalid file:",key)
                invalid_file = key

    f = open('./testfiles/'+invalid_file, 'r')
    body = f.readline()
    f.close()
    data, out = first.data_for_verification(body)
    print("Correct body:", data[1:20]+"....")
    print("Transferring correct file")
    print("Hashes for verification:",out)