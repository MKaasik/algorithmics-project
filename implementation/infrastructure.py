from timeit import default_timer as timer
import random

import numpy
import csv
import copy

def performance(function, iterations, array, arguments = ()):
    iterationAndTime = {}
    for i in range(1, iterations+1):
        A = copy.deepcopy(array)
        start = timer()
        function(A, *arguments)
        end = timer()
        iterationAndTime[i] = (end-start)
    print(function.__name__ + " " + str(iterations) + " iterations with N = " + str(len(array)))
    printStats(iterationAndTime)
    return iterationAndTime

def printStats(d):
    values = list(d.values())
    print ("Max: " + str(max(values)*1000) + "ms")
    print ("Min: " + str(min(values)*1000) + "ms")
    print ("Avg: " + str((sum(values)/len(values))*1000) + "ms")
    print ("Std: " + str(numpy.std(values)) + "ms")

def produceResultsList(d, size):
    values = list(d.values())
    return [size, max(values)*1000, min(values)*1000, ((sum(values)/len(values))*1000)]

def genRnd64BitInt():
    return random.getrandbits(64)

def genRnd32BitInt():
    return random.getrandbits(32)

def produceGnuplotFile(fnName, results):
    filename = fnName + ".txt"
    with open(filename, "w") as f:
        writer = csv.writer(f, delimiter=" ")
        writer.writerows(results)

def swap(a, x, y):
    temp = a[x]
    a[x] = a[y]
    a[y] = temp