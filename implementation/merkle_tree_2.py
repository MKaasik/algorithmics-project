from hashlib import md5
from math import log, ceil, floor
from timeit import default_timer as timer
import os
import sys
import infrastructure as inf

class MerkleTree:

    def get_hash(self, data):
        hashfn = md5()
        hashfn.update(bytes(str(data), encoding="UTF-8"))
        return hashfn.hexdigest()

    def __init__(self):
        self.root_hash = None
        self.height = None
        self.name_to_hash = {}
        self.hashes = []

    def from_hashes_list(self, hashlist):
        self.root_hash = hashlist[0]
        self.hashes = hashlist

    def build_tree(self):
        self.height = ceil(log(len(self.name_to_hash), 2))
        hashes_in_order = []
        for key in sorted(self.name_to_hash):
            hashes_in_order.append(self.name_to_hash[key])
        start = timer()
        diff = 2**self.height - len(hashes_in_order)
        empty_string_hash = self.get_hash("")
        hashes_in_order = hashes_in_order + ([empty_string_hash]*diff)
        number_of_nodes = (2 ** (self.height + 1)) - 1
        nodes_in_level = len(hashes_in_order)
        while len(hashes_in_order) != number_of_nodes:
            intermediate_list = []
            for i in range(0, nodes_in_level, 2):
                hash_of_hash = self.get_hash((hashes_in_order[i] + hashes_in_order[i+1]))
                intermediate_list.append(hash_of_hash)
            hashes_in_order = intermediate_list + hashes_in_order
            nodes_in_level //= 2

        end = timer()
        print("Time to build the tree: ",(end - start) * 1000, "ms")

        self.hashes = hashes_in_order
        self.root_hash = hashes_in_order[0]
        return (end - start) * 1000

    def add(self, name, data):
        data_hash = self.get_hash(data)
        self.name_to_hash[name] = data_hash

    def add_from_file(self, name, file_path):
        f = open(file_path, 'rb')
        md5fn = md5()
        while True:
            data = f.read(8192)
            if not data:
                break
            md5fn.update(data)
        data_hash = md5fn.hexdigest()
        self.name_to_hash[name] = data_hash

    def get_sibling(self, index):
        if index % 2 == 0:
            return index - 1
        return index + 1

    def get_uncle(self, index):
        parent = floor((index-1) / 2)
        if parent == 0:
            return None
        return self.get_sibling(int(parent))

    def get_children(self, index):
        first_child_index = index * 2 + 1
        return [first_child_index, first_child_index + 1]

    def get_children_hashes(self, hash):
        index_of_hash = self.hashes.index(hash)
        children = self.get_children(index_of_hash)
        return [self.hashes[children[0]], self.hashes[children[1]]]

    def data_for_verification(self, data):
        #returns data and its sibling + uncles for verification
        out = []
        hash = self.get_hash(data)

        data_index = self.hashes.index(hash) if hash in self.hashes else None
        if data_index is None:
            print(data, "Not in tree")
            return None
        index = self.get_sibling(data_index)
        sibling_hash = self.hashes[index]
        if index % 2 == 0:
            #for the add order
            out.append(sibling_hash)
        else:
            out.append('-' + sibling_hash)
        previous_hash = sibling_hash

        while True:
            index = self.get_uncle(index)
            if index == None:
                #all done if uncle is root
                break
            else:
                uncle_hash = self.hashes[index]
                if index % 2 == 0:
                    #for the add order
                    hash = self.get_hash(previous_hash + uncle_hash)
                    out.append(uncle_hash)
                else:
                    hash = self.get_hash(uncle_hash+previous_hash)
                    out.append('-'+uncle_hash)
                previous_hash = hash
        return data, out


# def merkle_tree_from_dir(dirname):
#     mt = MerkleTree()
#     for root, dirs, files in os.walk(dirname):
#         for file in files:
#             mt.add_from_file(file, os.path.join(root, file))
#     mt.build_tree()
#     return mt
#
# print(10*'-')
# first = merkle_tree_from_dir("./testfiles")
# print("Total no. of hashes:",len(first.hashes))
# print("Total no. of files:",len(first.name_to_hash))
# print("Root hash:", first.root_hash)
#
# print(10*'-')
# second = merkle_tree_from_dir("./testfiles2")
# print("Total no. of hashes:",len(second.hashes))
# print("Total no. of files:",len(second.name_to_hash))
# print("Root hash:", second.root_hash)
# print(10*'-')


# comparison of 2 merkle trees - returns list of hashes that differ from original tree (mt1)
def get_different_element(mt1, mt2, node_indexes, ret):
    for node_index in node_indexes:
        if(mt1.hashes[node_index] == mt2.hashes[node_index]):
            if(node_index == 0):
                break
        else:
            children = mt1.get_children(node_index)
            if(children[0] >= len(mt1.hashes) or children[1] >= len(mt1.hashes)):
                ret.append(mt2.hashes[node_index])
            else:
                get_different_element(mt1, mt2, mt1.get_children(node_index), ret)
    return ret


# computer B - mt1
# computer A - mt2
def protocol_simulation(mt1, mt2):
    start = timer()
    invalid_leafs = []
    a_sends_to_b = []
    a_hash = mt2.root_hash
    a_sends_to_b.append(a_hash)
    b_hash = mt1.root_hash
    if a_hash == b_hash:
        print("File verified, everything is OK")
        return []
    print("Difference in root hashes, computer B requests subtrees of",a_hash,"from computer A")
    a_subtrees = mt2.get_children_hashes(a_hash)
    a_sends_to_b += a_subtrees
    b_subtrees = mt1.get_children_hashes(b_hash)
    mt2_data_items = list(mt2.name_to_hash.values())
    while True:
        wrong_hashes = []
        for i in range(0, len(a_subtrees)):
            if a_subtrees[i] != b_subtrees[i]:
                if a_subtrees[i] in mt2_data_items:
                    invalid_leafs.append(b_subtrees[i])
                else:
                    print("Difference in hashes, computer B requests subtrees of", a_subtrees[i],"index",mt2.hashes.index(a_subtrees[i]), "from computer A")
                    wrong_hashes.append(a_subtrees[i])
        if not wrong_hashes:
            break
        else:
            a_subtrees = []
            b_subtrees = []
            for i in range(len(wrong_hashes)):
                a_subtrees += mt2.get_children_hashes(wrong_hashes[i])
                a_sends_to_b += a_subtrees
                index = mt2.hashes.index(wrong_hashes[i])
                b_subtrees += mt1.get_children_hashes(mt1.hashes[index])

    total_size_by_a = 0
    for a in a_sends_to_b:
        total_size_by_a += sys.getsizeof(a)
    print("Total hashes sent by A:",len(a_sends_to_b),"size:",total_size_by_a,"bytes")
    total_a_size = 0
    for x in mt2.name_to_hash.values():
        total_a_size += sys.getsizeof(x)
    print("Total size of all A hashes:",total_a_size,"bytes")
    end = timer()
    #return [(end-start)*1000, total_size_by_a/1024, total_a_size/1024]
    return invalid_leafs