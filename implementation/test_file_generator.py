import os
import random
import string

number_of_files = 100
number_of_random_letters_in_file = 256000

def directory_make_if_not_exists(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

test_dirs = ['testfiles', 'testfiles2']
directory_make_if_not_exists(test_dirs[0])
directory_make_if_not_exists(test_dirs[1])

def generate_files(number_of_files):
    for i in range(number_of_files):
        random_text = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(number_of_random_letters_in_file))
        for test_dir in test_dirs:
            f = open(test_dir + '/testfile'+str(i+1), 'w')
            f.write(random_text)
            f.close()

generate_files(number_of_files)