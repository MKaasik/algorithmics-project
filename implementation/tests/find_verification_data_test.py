import infrastructure as inf
import merkle_tree_2 as mt
import random
from timeit import default_timer as timer
import os

results = []
mt1 = mt.MerkleTree()
for i in range(1, 100000):
    mt1.add(str(i), "data_node"+str(i))
    if i % 2000 == 0:
        mt1.build_tree()
        avg = []
        for j in range(30):
            start = timer()
            mt1.data_for_verification("data_node"+str(random.randint(1,i)))
            end = timer()
            avg.append((end-start)*1000)
        results.append([i, (sum(avg)/len(avg))])

inf.produceGnuplotFile("../graph_data/find-verification-data", results)
