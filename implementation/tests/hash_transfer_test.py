import infrastructure as inf
import merkle_tree_2 as mt
from timeit import default_timer as timer
import os

data_nodes = [20000, 60000, 80000, 100000, 200000, 300000, 400000, 500000, 800000, 1000000]
results = []
mt1 = mt.MerkleTree()
mt2 = mt.MerkleTree()
for i in range(1, 500001):
    text = str(os.urandom(256)) + str(i)
    mt1.add(str(i), text)
    mt2.add(str(i), text)
    if i % 50000 == 0:
        mt1.build_tree()
        mt2.add(str(i), text+"tere")
        mt2.build_tree()
        res = mt.protocol_simulation(mt1,mt2)
        results.append([i, res[0], res[1], res[2]])

        # reset text
        mt2.add(str(i), text)

inf.produceGnuplotFile("../graph_data/hash-transfer", results)