import merkle_tree_2 as mt2
import infrastructure as inf
import os

tree = mt2.MerkleTree()
timings = []
for i in range(1, 25000):
    random_text = str(os.urandom(256)) + str(i)
    tree.add(str(i), random_text)
    if i % 200 == 0:
        avg = []
        for j in range(50):
            time_taken = tree.build_tree()
            avg.append(time_taken)
        print("Average:",(sum(avg)/len(avg)))
        timings.append([i, (sum(avg)/len(avg))])

inf.produceGnuplotFile("../graph_data/merkle-tree-build", timings)
