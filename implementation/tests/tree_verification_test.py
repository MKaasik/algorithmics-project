import infrastructure as inf
import merkle_tree_2 as mt
from timeit import default_timer as timer
import os

data_nodes = [10000, 20000, 30000, 50000, 80000, 100000, 200000, 300000, 400000, 500000, 800000, 1000000]
results = []
mt1 = mt.MerkleTree()
mt2 = mt.MerkleTree()
for i in range(1, 1000000):
    text = str(os.urandom(256)) + str(i)
    mt1.add(str(i), text)
    mt2.add(str(i), text)
    if i % 20000 == 0:
        print(i)
        mt1.build_tree()
        mt2.add(str(i), text+"tere")
        mt2.build_tree()
        avg_one = []
        for j in range(20):
            start = timer()
            mt.get_different_element(mt1, mt2, [0], [])
            end = timer()
            avg_one.append((end - start) * 1000)
        mt2.add(str(500), text+"tere")
        mt2.build_tree()
        avg_two = []
        for j in range(20):
            start = timer()
            mt.get_different_element(mt1, mt2, [0], [])
            end = timer()
            avg_two.append((end - start) * 1000)
        results.append([i, (sum(avg_one) / len(avg_one)), (sum(avg_two)/len(avg_two))])

        # reset text
        mt2.add(str(i), text)
        mt2.add(str(500), text)

inf.produceGnuplotFile("../graph_data/file-verification", results)