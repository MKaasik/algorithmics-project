from hashlib import md5
from math import log, ceil, floor
from os import listdir
from os.path import isfile, join

class MerkleTree:

    def get_hash(self, data):
        hashfn = md5()
        hashfn.update(bytes(data, encoding="UTF-8"))
        return hashfn.hexdigest()

    def __init__(self, data):
        self.root_hash = None
        if len(data) == 0:
            return

        self.height = ceil(log(len(data), 2))
        number_of_nodes = (2**(self.height+1)) - 1

        while len(data) != 2**self.height:
            data.append("empty_leaf")
        hashes = [self.get_hash(x) for x in data]
        nodes_in_level = len(data)
        while len(hashes) != number_of_nodes:
            intermediate_list = []
            for i in range(0, nodes_in_level, 2):
                hash_of_hash = self.get_hash((hashes[i] + hashes[i+1]))
                intermediate_list.append(hash_of_hash)
            hashes = intermediate_list + hashes
            nodes_in_level //= 2

        self.hashes = hashes
        self.root_hash = hashes[0]


    def sethashesandroothash(self, hashes):
        self.root_hash = hashes[0]
        self.hashes = hashes


    def get_sibling(self, index):
        if index % 2 == 0:
            return index - 1
        return index + 1

    def get_uncle(self, index):
        parent = floor((index-1) / 2)
        if parent == 0:
            return None
        return self.get_sibling(int(parent))

    def get_children(self, index):
        first_child_index = index * 2 + 1
        return [first_child_index, first_child_index + 1]

    def verify(self, data, lst):
        #Checks if given data is in the tree
        #for that it needs list of sibling and uncle(s') hashes
        hash = self.get_hash(data)
        for el in lst:
            if el[0] == '-':
                #add order matters, therefore it must be specified by sender
                hash = self.get_hash(el[1:] + hash)
            else:
                hash = self.get_hash(hash + el)
        #    print(hash)

        print(data, "is in tree") if hash == self.root_hash else print(data, "not in tree")

    def data_for_verification(self, data):
        #returns data and its sibling + uncles for verification
        out = []
        hash = self.get_hash(data)

        data_index = self.hashes.index(hash) if hash in self.hashes else None
        if data_index is None:
            print(data, "not in tree")
            return None
        index = self.get_sibling(data_index)
        sibling_hash = self.hashes[index]
        if index % 2 == 0:
            #for the add order
            out.append(sibling_hash)
        else:
            out.append('-' + sibling_hash)
        previous_hash = sibling_hash

        while True:
            index = self.get_uncle(index)
            if index == None:
                #all done if uncle is root
                break
            else:
                uncle_hash = self.hashes[index]
                if index % 2 == 0:
                    #for the add order
                    hash = self.get_hash(previous_hash + uncle_hash)
                    out.append(uncle_hash)
                else:
                    hash = self.get_hash(uncle_hash+previous_hash)
                    out.append('-'+uncle_hash)
                previous_hash = hash
        return data, out

    def find(self, data):
        hash = self.get_hash(data)
        i = self.hashes.index(hash) if hash in self.hashes else None
        print(i)
        if i is None:
            print("NOT INCLUDED")
            return;
        sibling = self.get_sibling(i)
        kaks = self.hashes[sibling]
        print(sibling, kaks)
        ykskaks = self.get_hash(hash + kaks)
        sibling = self.get_sibling(self.hashes.index(ykskaks))
        kolmneli = self.hashes[sibling]

    #def compare_hashes(self, hashes):


d1 = ["üks", "kaks", "kolm", "neli", "viis"]
d2 = ["üks", "akaks", "12kolm", "neli", "viis"]

mt1 = MerkleTree(d1)
mt2 = MerkleTree(d2)

def get_different_element(mt1, mt2, node_indexes, ret):
    for node_index in node_indexes:
        if(mt1.hashes[node_index] == mt2.hashes[node_index]):
            if(node_index == 0):
            #    print("Trees are same")
                break
        else:
            children = mt1.get_children(node_index)
            if(children[0] >= len(mt1.hashes) or children[1] >= len(mt1.hashes)):
            #    print("BROKEN NODE", node_index)
                ret.append(mt2.hashes[node_index])
            else:
                get_different_element(mt1, mt2, mt1.get_children(node_index), ret)
    return ret

#verify
#print("Obtaining data for verification...")
#verification = mt1.data_for_verification("kolm")
#print(verification)
#print("Verifying data")
#mt1.verify("kolm", verification[1])

print(get_different_element(mt1, mt2, [0], []))

#mt1.verify("neli", ['-c129c8117655dbf88020107f27705035', 'cd -627ef76a1c9d936994e762ed6f614f1b'])

print("PC1 sends correct verification procedure. \n")
print("PC1: Obtaining data for verification...")
verification = mt1.data_for_verification("kolm")
print("PC1: this is data to verify that I'm not lying:")
print("\t",verification)
print("PC2: Checking what PC1 sent...")
mt1.verify("kolm", verification[1])
print("PC2: Verification done!")

print(10*'-')
print("PC1 sends files that are not what PC2 has. \n")
print("PC1: hei, are files, acccept it y0")
print("\t files PC1 has", d1)
print("PC2: one moment, i'll compare the Merkle trees")
print("\t files PC2 has", d2)
get_different_element(mt1, mt2, [0])
print("PC2: Merkle trees compared")

print(10*'-')
print("PC1 sends files that PC2 has but the verification info is wrong.\n")
print("PC1: hei, here is a file")
print("PC2: checking this file")
mt1.verify("neli", ['-c129c8117655dbf88020107f27705035', 'cd -627ef76a1c9d936994e762ed6f614f1b'])
print("PC2: Verification done")
print(10*'-')

randommap = {mt1.root_hash: mt1}

def find_in_merkle(data, root):
    merkle = randommap[root]
    hash = merkle.get_hash(data)
    while True:
        if hash == root:
            #print("IS INCLUDED")
            break;
        data_index = merkle.hashes.index(hash) if hash in merkle.hashes else None
        if data_index is None:
            print("NOT INCLUDED")
            break;
        sibling_index = merkle.get_sibling(data_index)
        sibling_hash = merkle.hashes[sibling_index]
        hash = merkle.get_hash(hash + sibling_hash if sibling_index % 2 == 0 else sibling_hash + hash)

#find_in_merkle("üks", 'b6dc0b09a037879be2c16c199e4028c8')

mypath = "./testfolder"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
t = []
for o in onlyfiles:
    with open('testfolder/'+o, 'r') as f:
        t.append(f.read().replace('\n', ''))

mt = MerkleTree(t)
print(mt.root_hash)

mypath = "./testfolder2"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
t = []
for o in onlyfiles:
    with open('testfolder2/'+o, 'r') as f:
        t.append(f.read().replace('\n', ''))

mt2 = MerkleTree(t)
print(mt2.root_hash)